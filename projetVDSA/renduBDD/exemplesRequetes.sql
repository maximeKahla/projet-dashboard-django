-- Nous effectuerons ces requêtes lorsque la base de données sera remplies.
-- Voici une liste de requetes non exaustifs

-- Selection tout les pseudonymes des Administrateur (Gestion des droits). Pour récupéré les pseudonymes des Directeurs il faut remplacer le 1 par 2 et pour les Commerciaux il faut remplacer le 1 par 3.
SELECT pseudonyme FROM Utilisateur WHERE statut = 1; 

-- Ajouter un produit (en considérant que la famille existe déjà)
INSERT INTO Sous_famille (id_sous_famille,id_famille) VALUES ("SERVICES","DIVERS");

-- Récupération des factures pour le vendeur numéro 3
SELECT * FROM Facture WHERE id_user = 3 ;