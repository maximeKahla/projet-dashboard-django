CREATE TABLE Utilisateur(
   id_user INT UNIQUE NOT NULL,
   statut INT,
   mot_de_passe VARCHAR(50),
   pseudonyme VARCHAR(50),
   PRIMARY KEY (id_user)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE Client(
   code_client VARCHAR(50) UNIQUE NOT NULL,
   code_postal INT NOT NULL,
   ville VARCHAR(50),
   PRIMARY KEY (code_client)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE Famille(
   id_famille VARCHAR(50) UNIQUE NOT NULL,
   PRIMARY KEY (id_famille)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE Sous_famille(
   id_sous_famille VARCHAR(50) UNIQUE NOT NULL,
   id_famille VARCHAR(50),
   PRIMARY KEY (id_sous_famille),
   FOREIGN KEY (id_famille) REFERENCES Famille(id_famille)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE Facture(
   id_facture INT UNIQUE NOT NULL,
   prix_facture DECIMAL(15,2),
   marge DECIMAL(15,2),
   date_facture DATETIME,
   code_client VARCHAR(50),
   id_user INT,
   id_sous_famille VARCHAR(50),
   PRIMARY KEY (id_facture), 
   FOREIGN KEY (id_user) REFERENCES Utilisateur(id_user),
   FOREIGN KEY (code_client) REFERENCES Client(code_client),
   FOREIGN KEY (id_sous_famille) REFERENCES Sous_famille(id_sous_famille)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;