# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import date

# Create your models here.

class Client(models.Model):
    code_client = models.CharField(max_length = 10, primary_key = True)
    def __str__(self):
        return self.code_client

class Type(models.Model):
    id_type = models.IntegerField(primary_key = True)
    libelle_type = models.CharField(max_length = 30)
    def __str__(self):
        return self.libelle_type

class Famille(models.Model):
    id_famille = models.CharField(max_length = 10, primary_key = True)
    libelle_famille = models.CharField(max_length = 50, default = "INDEFINI")
    def __str__(self):
        return self.libelle_famille

class Sous_famille(models.Model):
    id_sous_famille = models. IntegerField(primary_key = True)
    libelle_sous_famille = models.CharField(max_length = 50)
    id_famille = models.ForeignKey('Famille', on_delete = models.PROTECT, default = 0)
    def __str__(self):
        return self.libelle_sous_famille

class Utilisateur(models.Model):
    id_utilisateur = models.IntegerField(primary_key = True)
    id_type = models.ForeignKey('Type', on_delete = models.PROTECT)
    mot_de_passe = models.CharField(max_length = 50, default = "utilisateur00")
    pseudonyme = models.CharField(max_length = 50, default = "utilisateur")
    def __str__(self):
        return self.pseudonyme

class Facture(models.Model):
    prix_facture = models.DecimalField(null = True, default = None, max_digits = 8, decimal_places = 2)
    marge_facture = models.DecimalField(null = True, default = None, max_digits = 8, decimal_places = 2)
    date_facture = models.DateField(null = True, default = date.today, verbose_name = "Date facture")
    code_postal = models.IntegerField(null = True, default = None)
    ville = models.CharField(max_length=50, null = True)
    id_client = models.ForeignKey('Client', on_delete = models.PROTECT)
    id_sous_famille = models.ForeignKey('Sous_famille', on_delete = models.PROTECT)
    id_utilisateur = models.ForeignKey('Utilisateur', null = 0, on_delete = models.PROTECT)
    def __str__(self):
        return self.id_client.code_client
