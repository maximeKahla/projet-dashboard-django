from django import forms

class ConnexionForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur",  required="required",max_length=30)
    password = forms.CharField(label="Mot de passe" ,required="required",  widget=forms.PasswordInput)