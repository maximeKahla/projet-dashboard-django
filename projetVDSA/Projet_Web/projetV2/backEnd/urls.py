from django.urls import path, include
from django.contrib import admin
from . import views
from django.views.generic.base import TemplateView


urlpatterns = [
    # path('index',views.index,name="index"),
    # path('main',views.home,name="main"),
    path('admin2/', admin.site.urls,name='admin2'),
    path('accounts/',include('django.contrib.auth.urls')),
    #path('',TemplateView.as_view(template_name='home.html'),name='test'),
    path('',views.home,name='home')
]