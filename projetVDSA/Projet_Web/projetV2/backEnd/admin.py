from django.contrib import admin

from .models import Client
from .models import Famille
from .models import Sous_famille
from .models import Type
from .models import Utilisateur
from .models import Facture

# Register your models here.
admin.site.register(Client)
admin.site.register(Famille)
admin.site.register(Sous_famille)
admin.site.register(Type)
admin.site.register(Utilisateur)
admin.site.register(Facture)
