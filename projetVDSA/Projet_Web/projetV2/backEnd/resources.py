from core.models import Client

class ClientResource(resources.ModelResource):
    class Meta:
        model = Client
        fields=['code_client']
