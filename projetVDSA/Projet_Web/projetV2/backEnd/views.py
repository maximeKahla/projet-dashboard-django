from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import Client
from .models import Famille
from .models import Sous_famille
from .models import Facture
from .models import Utilisateur
from django.db.models import Sum, Avg , Count
from django.shortcuts import render_to_response

import datetime

import csv

             
def home(request): 
    
    nb_client=Client.objects.count()
    client = Client.objects.all()
    famille=Famille.objects.all()
    sous_famille=Sous_famille.objects.all()
    facture=Facture.objects.filter(id_client = client[0]).aggregate(chiffre_affaire = Sum('prix_facture')).values()
    utilisateur = Utilisateur.objects.all()
    utilisateurAct=utilisateur[7].id_utilisateur
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # PARTIE SUR LES CLIENTS


    tab_ca_2017=[]
    tab_ca_2016=[]
    tab_ca_2015=[]
    tab_m_2017=[]
    tab_m_2016=[]
    tab_m_2015=[]
    tab_m20_2017=[]
    #tab_m20_2016=[]
    #tab_m20_2015=[]

    for i in range(len(client)):
        tab_ca_2017.append((list(Facture.objects.filter(id_client = client[i],date_facture__year = 2017).aggregate(chiffre_affaire = Sum('prix_facture')).values())[0]))
        tab_ca_2016.append((list(Facture.objects.filter(id_client = client[i],date_facture__year = 2016).aggregate(chiffre_affaire = Sum('prix_facture')).values())[0]))
        tab_ca_2015.append((list(Facture.objects.filter(id_client = client[i],date_facture__year = 2015).aggregate(chiffre_affaire = Sum('prix_facture')).values())[0]))
        tab_m_2017.append(Facture.objects.filter(id_client = client[i],date_facture__year = 2017).aggregate(marge = Avg('marge_facture')).values())
        tab_m_2016.append(Facture.objects.filter(id_client = client[i],date_facture__year = 2016).aggregate(marge = Avg('marge_facture')).values())
        tab_m_2015.append(Facture.objects.filter(id_client = client[i],date_facture__year = 2015).aggregate(marge = Avg('marge_facture')).values())
        tab_m20_2017.append(Facture.objects.filter(id_client = client[i],date_facture__year = 2017, marge_facture__lt = 20).aggregate(marge = Avg('marge_facture')).values())
        #tab_m20_2016.append(Facture.objects.filter(id_client = client[i],date_facture__year = 2016, marge_facture < 20).aggregate(marge = Avg('marge_facture')))[0])
        #tab_m20_2015.append(Facture.objects.filter(id_client = client[i],date_facture__year = 2015, marge_facture < 20).aggregate(marge = Avg('marge_facture')))[0])

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # PARTIE SUR LES SOUS_FAMILLE

    sous_famille = Sous_famille.objects.all()
    tab_ca_sfam_2017=[]
    tab_ca_sfam_2016=[]
    tab_ca_sfam_2015=[]
    tab_m_sfam_2017=[]
    tab_m_sfam_2016=[]
    tab_m_sfam_2015=[]

    for i in range(len(sous_famille)):
        tab_ca_sfam_2017.append(list(Facture.objects.filter(id_sous_famille = sous_famille[i],date_facture__year = 2017).aggregate(chiffre_affaire = Sum('prix_facture')).values())[0])
        tab_ca_sfam_2016.append(list(Facture.objects.filter(id_sous_famille = sous_famille[i],date_facture__year = 2016).aggregate(chiffre_affaire = Sum('prix_facture')).values())[0])
        tab_ca_sfam_2015.append(list(Facture.objects.filter(id_sous_famille = sous_famille[i],date_facture__year = 2015).aggregate(chiffre_affaire = Sum('prix_facture')).values())[0])
        tab_m_sfam_2017.append(list(Facture.objects.filter(id_sous_famille = sous_famille[i],date_facture__year = 2017).aggregate(marge = Avg('marge_facture')).values())[0])
        tab_m_sfam_2016.append(list(Facture.objects.filter(id_sous_famille = sous_famille[i],date_facture__year = 2016).aggregate(marge = Avg('marge_facture')).values())[0])
        tab_m_sfam_2015.append(list(Facture.objects.filter(id_sous_famille = sous_famille[i],date_facture__year = 2015).aggregate(marge = Avg('marge_facture')).values())[0])


    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # POURCENTAGE CLIENT
    acc_client_2015=0
    for i in tab_ca_2015:
        if (i != None):
            acc_client_2015=acc_client_2015+1

    acc_client_2016=0
    for i in tab_ca_2016:
        if (i != None):
            acc_client_2016=acc_client_2016+1
    pourcentage_client_2016 = 100 * ((acc_client_2016/float(acc_client_2015)) - 1)
    pourcentage_client_2016 = round(pourcentage_client_2016, 2)

    acc_client_2017=0
    for i in tab_ca_2017:
        if (i != None):
            acc_client_2017=acc_client_2017+1
    pourcentage_client_2017 = 100 * ((acc_client_2017/float(acc_client_2016)) - 1)
    pourcentage_client_2017 = round(pourcentage_client_2017, 2)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # POURCENTAGE CA
    ca_2015=list(Facture.objects.filter(date_facture__year = 2015).aggregate(CA = Sum('prix_facture')).values())[0]

    ca_2016=list(Facture.objects.filter(date_facture__year = 2016).aggregate(CA = Sum('prix_facture')).values())[0]
    pourcentage_ca_2016 = 100 * ((ca_2016 / ca_2015) - 1)
    pourcentage_ca_2016 = round(pourcentage_ca_2016, 2)

    ca_2017 = list(Facture.objects.filter(date_facture__year = 2017).aggregate(CA = Sum('prix_facture')).values())[0]
    pourcentage_ca_2017 = 100 * ((ca_2017 / ca_2016) - 1)
    pourcentage_ca_2017 = round(pourcentage_ca_2017, 2)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # POURCENTAGE MARGE
    marge_2015=list(Facture.objects.filter(date_facture__year = 2015).aggregate(marge = Avg('marge_facture')).values())[0]
    marge_2015=round(marge_2015,2)

    marge_2016=list(Facture.objects.filter(date_facture__year = 2016).aggregate(marge = Avg('marge_facture')).values())[0]
    pourcentage_marge_2016 = 100 * ((marge_2016 / marge_2015) - 1)
    pourcentage_marge_2016=round(pourcentage_marge_2016,2)

    marge_2017 = list(Facture.objects.filter(date_facture__year = 2017).aggregate(marge = Avg('marge_facture')).values())[0]
    pourcentage_marge_2017 = 100 * ((marge_2017 / marge_2016) - 1)
    pourcentage_marge_2017=round(pourcentage_marge_2017,2)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # POURCENTAGE FACTURE
    facture_2015=Facture.objects.filter(date_facture__year = 2015).count()

    facture_2016=Facture.objects.filter(date_facture__year = 2016).count()
    pourcentage_facture_2016 = 100 * ((facture_2016 / float(facture_2015)) - 1)
    pourcentage_facture_2016=round(pourcentage_facture_2016,2)

    facture_2017=Facture.objects.filter(date_facture__year = 2017).count()
    pourcentage_facture_2017 = 100 * ((facture_2017 / float(facture_2016)) - 1)
    pourcentage_facture_2017=round(pourcentage_facture_2017,2)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # LINE CHART CHART CA PAR MOIS PAR ANNEE
    tab_ca_mois_2015=[]
    tab_ca_mois_2016=[]
    tab_ca_mois_2017=[]

    for i in range(12):
        tab_ca_mois_2015.append(int(list(Facture.objects.filter(date_facture__year=2015 ,date_facture__month = i+1).aggregate(CA = Sum('prix_facture')).values())[0]))
        tab_ca_mois_2016.append(int(list(Facture.objects.filter(date_facture__year=2016 ,date_facture__month = i+1).aggregate(CA = Sum('prix_facture')).values())[0]))
        tab_ca_mois_2017.append(int(list(Facture.objects.filter(date_facture__year=2017 ,date_facture__month = i+1).aggregate(CA = Sum('prix_facture')).values())[0]))



    #€€€€€€€€€€€€€€€€€€€€€€€€€€€€€ KP
    #ca_2017_Utilisateur = list(Facture.objects.filter(date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_2017_Utilisateur = list(Facture.objects.filter(date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_2016_Utilisateur = list(Facture.objects.filter(date_facture__year = 2016, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_2015_Utilisateur = list(Facture.objects.filter(date_facture__year = 2015, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    nb_facture_2017_Utilisateur=Facture.objects.filter(date_facture__year = 2017, id_utilisateur=utilisateurAct).count()
   


    ca_Mag_V_2017 = list(Facture.objects.filter(date_facture__year = 2017, id_client__code_client__startswith = 'V').aggregate(CA =Sum('prix_facture')).values())[0]
    ca_Mag_D_2017 = list(Facture.objects.filter(date_facture__year = 2017, id_client__code_client__startswith = 'D').aggregate(CA =Sum('prix_facture')).values())[0]

   
    ca_Jan_Utilisateur = list(Facture.objects.filter(date_facture__month = 1, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Fev_Utilisateur = list(Facture.objects.filter(date_facture__month = 2, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Mar_Utilisateur = list(Facture.objects.filter(date_facture__month = 3, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Avr_Utilisateur = list(Facture.objects.filter(date_facture__month = 4, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Mai_Utilisateur = list(Facture.objects.filter(date_facture__month = 5, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Juin_Utilisateur = list(Facture.objects.filter(date_facture__month = 6, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Juil_Utilisateur = list(Facture.objects.filter(date_facture__month = 7, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Aou_Utilisateur = list(Facture.objects.filter(date_facture__month = 8, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Sep_Utilisateur = list(Facture.objects.filter(date_facture__month = 9, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Oct_Utilisateur = list(Facture.objects.filter(date_facture__month = 10, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Nov_Utilisateur = list(Facture.objects.filter(date_facture__month = 11, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]
    ca_Dec_Utilisateur = list(Facture.objects.filter(date_facture__month = 12, date_facture__year = 2017, id_utilisateur = utilisateurAct).aggregate(CA = Sum('prix_facture')).values())[0]

    if ca_Jan_Utilisateur is None:
        ca_Jan_Utilisateur = 0
    if ca_Fev_Utilisateur is None:
        ca_Fev_Utilisateur = 0
    if ca_Mar_Utilisateur is None:
        ca_Mar_Utilisateur = 0
    if ca_Avr_Utilisateur is None:
        ca_Avr_Utilisateur = 0
    if ca_Mai_Utilisateur is None:
        ca_Mai_Utilisateur = 0
    if ca_Juin_Utilisateur is None:
        ca_Juin_Utilisateur = 0
    if ca_Juil_Utilisateur is None:
        ca_Juil_Utilisateur = 0
    if ca_Aou_Utilisateur is None:
        ca_Aou_Utilisateur = 0
    if ca_Sep_Utilisateur is None:
        ca_Sep_Utilisateur = 0
    if ca_Oct_Utilisateur is None:
        ca_Oct_Utilisateur = 0
    if ca_Nov_Utilisateur is None:
        ca_Nov_Utilisateur = 0
    if ca_Dec_Utilisateur is None:
        ca_Dec_Utilisateur = 0


    class clientCAAnnee:
        def __init__(self, client , tab_2015, tab_2016, tab_2017):
            self.client = client
            self.tab_2015 = tab_2015
            self.tab_2016 = tab_2016
            self.tab_2017 = tab_2017

    tab_clientCAAnnee=[]
    for i in range(len(client)):
        x = clientCAAnnee(client[i].code_client,tab_ca_2015[i],tab_ca_2016[i],tab_ca_2017[i])
        tab_clientCAAnnee.append(x)


    #executions
    # print(var_afficheNombreClient)
    return render(request,"home.html", {"client": client,"famille":famille, "facture":facture,"sous_famille":sous_famille, "sous_famille":sous_famille,
                                                "tab_ca_2017":tab_ca_2017,"tab_ca_2016":tab_ca_2016,"tab_ca_2015":tab_ca_2015,
                                                "tab_m_2017":tab_m_2017,"tab_m_2016":tab_m_2016,"tab_m_2015":tab_m_2015,
                                                "tab_m_sfam_2017":tab_m_sfam_2017,"tab_m_sfam_2016":tab_m_sfam_2016,"tab_m_sfam_2015":tab_m_sfam_2015,
                                                "tab_ca_sfam_2017":tab_ca_sfam_2017,"tab_ca_sfam_2016":tab_ca_sfam_2016,"tab_ca_sfam_2015":tab_ca_sfam_2015,
                                                "tab_ca_mois_2017":tab_ca_mois_2017,"tab_ca_mois_2016":tab_ca_mois_2016,"tab_ca_mois_2015":tab_ca_mois_2015,
                                                "acc_client_2015":acc_client_2015,"acc_client_2016":acc_client_2016,"acc_client_2017":acc_client_2017,
                                                "pourcentage_client_2016":pourcentage_client_2016,"pourcentage_client_2017":pourcentage_client_2017,
                                                "nb_client":nb_client,
                                                "ca_2015":ca_2015,"ca_2016":ca_2016,"ca_2017":ca_2017,
                                                "pourcentage_ca_2016":pourcentage_ca_2016,"pourcentage_ca_2017":pourcentage_ca_2017,
                                                "marge_2015":marge_2015,"marge_2016":marge_2016,"marge_2017":marge_2017,
                                                "pourcentage_marge_2016":pourcentage_marge_2016,"pourcentage_marge_2017":pourcentage_marge_2017,
                                                "facture_2017":facture_2017,"facture_2016":facture_2016,"facture_2015":facture_2015,
                                                "pourcentage_facture_2016":pourcentage_facture_2016,"pourcentage_facture_2017":pourcentage_facture_2017,
                                                "ca_Jan_Utilisateur":ca_Jan_Utilisateur,"ca_Fev_Utilisateur":ca_Fev_Utilisateur,"ca_Mar_Utilisateur":ca_Mar_Utilisateur,
                                                "ca_Avr_Utilisateur":ca_Avr_Utilisateur,"ca_Mai_Utilisateur":ca_Mai_Utilisateur,"ca_Juin_Utilisateur":ca_Juin_Utilisateur,
                                                "ca_Juil_Utilisateur":ca_Juil_Utilisateur,"ca_Aou_Utilisateur":ca_Aou_Utilisateur,"ca_Sep_Utilisateur":ca_Sep_Utilisateur,
                                                "ca_Oct_Utilisateur":ca_Oct_Utilisateur,"ca_Nov_Utilisateur":ca_Nov_Utilisateur,"ca_Dec_Utilisateur":ca_Dec_Utilisateur,
                                                "ca_Mag_V_2017":ca_Mag_V_2017,"ca_Mag_D_2017":ca_Mag_D_2017,"ca_2017_Utilisateur":ca_2017_Utilisateur,
                                                "nb_facture_2017_Utilisateur":nb_facture_2017_Utilisateur,"ca_2016_Utilisateur":ca_2016_Utilisateur,"ca_2015_Utilisateur":ca_2015_Utilisateur,
                                                "tab_clientCAAnnee":tab_clientCAAnnee}
                                                )
