# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-01-13 21:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backEnd', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='famille',
            name='id_famille',
            field=models.IntegerField(default=0, primary_key=True, serialize=False),
        ),
    ]
