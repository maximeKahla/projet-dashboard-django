# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-01-13 22:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backEnd', '0003_auto_20190113_2141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='type',
            name='libelle_type',
            field=models.CharField(max_length=30),
        ),
    ]
