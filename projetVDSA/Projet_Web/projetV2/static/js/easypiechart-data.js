$(function() {
    $('#easypiechart-teal').easyPieChart({
        scaleColor: false,
        barColor: '#1ebfae'
    });
});

$(function() {
    $('#easypiechart-teal1').easyPieChart({
        scaleColor: false,
        barColor: '#1ebfae'
    });
});

$(function() {
    $('#easypiechart-teal2').easyPieChart({
        scaleColor: false,
        barColor: '#1ebfae'
    });
});


$(function() {
    $('#easypiechart-orange').easyPieChart({
        scaleColor: false,
        barColor: '#ffb53e'
    });
});

$(function() {
    $('#easypiechart-orange1').easyPieChart({
        scaleColor: false,
        barColor: '#ffb53e'
    });
});

$(function() {
    $('#easypiechart-violet').easyPieChart({
        scaleColor: false,
        barColor: '#DDA0DD'
    });
});

$(function() {
    $('#easypiechart-violet1').easyPieChart({
        scaleColor: false,
        barColor: '#DDA0DD'
    });
});

$(function() {
   $('#easypiechart-blue').easyPieChart({
       scaleColor: false,
       barColor: '#30a5ff'
   });
});

$(function() {
   $('#easypiechart-blue1').easyPieChart({
       scaleColor: false,
       barColor: '#30a5ff'
   });
});
