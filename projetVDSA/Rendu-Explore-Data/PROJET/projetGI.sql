-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2018 at 09:50 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetGI`
--

-- --------------------------------------------------------

--
-- Table structure for table `Client`
--

CREATE TABLE `Client` (
  `id_client` int(11) NOT NULL,
  `code_client` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Client`
--

INSERT INTO `Client` (`id_client`, `code_client`) VALUES
(1, '1'),
(2, '2'),
(3, 'D0003');

-- --------------------------------------------------------

--
-- Table structure for table `Facture`
--

CREATE TABLE `Facture` (
  `id_facture` int(11) NOT NULL,
  `prix_facture` decimal(15,2) DEFAULT NULL,
  `marge` decimal(15,2) DEFAULT NULL,
  `date_facture` datetime DEFAULT NULL,
  `code_postal` int(11) NOT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `id_client` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `id_sous_famille` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Facture`
--

INSERT INTO `Facture` (`id_facture`, `prix_facture`, `marge`, `date_facture`, `code_postal`, `ville`, `id_client`, `id_utilisateur`, `id_sous_famille`) VALUES
(1, '100.00', '15.60', '2018-12-16 00:00:00', 64000, 'Pau', 1, 1, 1),
(2, '200.00', '15.00', '2018-12-12 00:00:00', 64000, 'Pau', 2, 1, 1),
(3, '500.00', '16.00', '2017-11-10 00:00:00', 64000, 'Pau', 1, 1, 1),
(4, '400.00', '5.00', '2017-09-13 00:00:00', 64000, 'Pau', 2, 1, 2),
(5, '55.00', '55.00', '2018-12-16 00:00:00', 6, 'Pau', 2, 1, 2),
(6, '566.00', '13.00', '2019-01-10 00:00:00', 64, 'Pau', 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Famille`
--

CREATE TABLE `Famille` (
  `id_famille` int(11) NOT NULL,
  `libelle_famille` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Famille`
--

INSERT INTO `Famille` (`id_famille`, `libelle_famille`) VALUES
(1, 'Famille1'),
(2, 'Famille2');

-- --------------------------------------------------------

--
-- Table structure for table `Sous_famille`
--

CREATE TABLE `Sous_famille` (
  `id_sous_famille` int(11) NOT NULL,
  `libelle_sous_famille` varchar(50) NOT NULL,
  `id_famille` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Sous_famille`
--

INSERT INTO `Sous_famille` (`id_sous_famille`, `libelle_sous_famille`, `id_famille`) VALUES
(1, 'SFamille1', 1),
(2, 'SFamille2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `mot_de_passe` varchar(50) DEFAULT NULL,
  `pseudonyme` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Utilisateur`
--

INSERT INTO `Utilisateur` (`id_utilisateur`, `id_type`, `mot_de_passe`, `pseudonyme`) VALUES
(1, 3, 'toto', 'tata'),
(2, 2, 'toto', 'guillaume');

-- --------------------------------------------------------

--
-- Table structure for table `Utilisateur_type`
--

CREATE TABLE `Utilisateur_type` (
  `id_type` int(11) NOT NULL,
  `libelle_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Utilisateur_type`
--

INSERT INTO `Utilisateur_type` (`id_type`, `libelle_type`) VALUES
(1, 'Administrateur'),
(2, 'Directeurs de magasins'),
(3, 'Commercial');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`id_client`),
  ADD UNIQUE KEY `id_client` (`id_client`),
  ADD UNIQUE KEY `code_client` (`code_client`);

--
-- Indexes for table `Facture`
--
ALTER TABLE `Facture`
  ADD PRIMARY KEY (`id_facture`),
  ADD UNIQUE KEY `id_facture` (`id_facture`),
  ADD KEY `id_utilisateur` (`id_utilisateur`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_sous_famille` (`id_sous_famille`);

--
-- Indexes for table `Famille`
--
ALTER TABLE `Famille`
  ADD PRIMARY KEY (`id_famille`),
  ADD UNIQUE KEY `id_famille` (`id_famille`);

--
-- Indexes for table `Sous_famille`
--
ALTER TABLE `Sous_famille`
  ADD PRIMARY KEY (`id_sous_famille`),
  ADD UNIQUE KEY `id_sous_famille` (`id_sous_famille`),
  ADD KEY `id_famille` (`id_famille`);

--
-- Indexes for table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`id_utilisateur`),
  ADD UNIQUE KEY `id_utilisateur` (`id_utilisateur`),
  ADD UNIQUE KEY `id_type` (`id_type`);

--
-- Indexes for table `Utilisateur_type`
--
ALTER TABLE `Utilisateur_type`
  ADD PRIMARY KEY (`id_type`),
  ADD UNIQUE KEY `id_type` (`id_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Client`
--
ALTER TABLE `Client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Facture`
--
ALTER TABLE `Facture`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Famille`
--
ALTER TABLE `Famille`
  MODIFY `id_famille` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Sous_famille`
--
ALTER TABLE `Sous_famille`
  MODIFY `id_sous_famille` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Utilisateur_type`
--
ALTER TABLE `Utilisateur_type`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Facture`
--
ALTER TABLE `Facture`
  ADD CONSTRAINT `Facture_ibfk_1` FOREIGN KEY (`id_utilisateur`) REFERENCES `Utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `Facture_ibfk_2` FOREIGN KEY (`id_client`) REFERENCES `Client` (`id_client`),
  ADD CONSTRAINT `Facture_ibfk_3` FOREIGN KEY (`id_sous_famille`) REFERENCES `Sous_famille` (`id_sous_famille`);

--
-- Constraints for table `Sous_famille`
--
ALTER TABLE `Sous_famille`
  ADD CONSTRAINT `Sous_famille_ibfk_1` FOREIGN KEY (`id_famille`) REFERENCES `Famille` (`id_famille`);

--
-- Constraints for table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD CONSTRAINT `Utilisateur_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `Utilisateur_type` (`id_type`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
